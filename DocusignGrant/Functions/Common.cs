﻿//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Security.Cryptography;
//using System.Text;
//using System.Threading.Tasks;
//using System.Xml;

//namespace DocusignGrant.Functions
//{
//    public class Common
//    {
//        //public string Sign(string payload, string privateKey)
//        //{
//        //    IList segments = new List();
//        //    var header = new { alg = "RS256", typ = "JWT" };

//        //    DateTime issued = DateTime.Now;
//        //    DateTime expire = DateTime.Now.AddHours(10);

//        //    byte[] headerBytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(header, Newtonsoft.Json.Formatting.None));
//        //    byte[] payloadBytes = Encoding.UTF8.GetBytes(payload);

//        //    segments.Add(Base64UrlEncode(headerBytes));
//        //    segments.Add(Base64UrlEncode(payloadBytes));

//        //    string bodyEncoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(header));
//        //    string headerEncoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(payload));

//        //    string stringToSign = string.Join(".", segments.ToArray());

//        //    byte[] bytesToSign = Encoding.UTF8.GetBytes(stringToSign);

//        //    byte[] keyBytes = Convert.FromBase64String(privateKey);

//        //    var privKeyObj = Asn1Object.FromByteArray(keyBytes);
//        //    var privStruct = RsaPrivateKeyStructure.GetInstance((Asn1Sequence)privKeyObj);

//        //    ISigner sig = SignerUtilities.GetSigner("SHA256withRSA");

//        //    sig.Init(true, new RsaKeyParameters(true, privStruct.Modulus, privStruct.PrivateExponent));

//        //    sig.BlockUpdate(bytesToSign, 0, bytesToSign.Length);
//        //    byte[] signature = sig.GenerateSignature();

//        //    segments.Add(Base64UrlEncode(signature));
//        //    return string.Join(".", segments.ToArray());
//        //}
//        public static byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
//        {
//            try
//            {
//                byte[] encryptedData;
//                //Create a new instance of RSACryptoServiceProvider.
//                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
//                {

//                    //Import the RSA Key information. This only needs
//                    //toinclude the public key information.
//                    RSA.ImportParameters(RSAKeyInfo);

//                    //Encrypt the passed byte array and specify OAEP padding.  
//                    //OAEP padding is only available on Microsoft Windows XP or
//                    //later.  
//                    encryptedData = RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
//                }
//                return encryptedData;
//            }
//            //Catch and display a CryptographicException  
//            //to the console.
//            catch (CryptographicException e)
//            {
//                Console.WriteLine(e.Message);

//                return null;
//            }
//        }

//    }
//}
