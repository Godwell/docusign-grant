﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DocusignGrant.Models;
using System.Text.Json;
using System.Text;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Net;
using System.IO;

namespace DocusignGrant.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            
            _logger = logger;
        }

        public IActionResult Index()
        {
            //This is the redirect url
            //https://account-d.docusign.com/oauth/auth?response_type=code&scope=signature&client_id=0e7585c5-11e4-4111-8bed-c81cc16ab207&state=a39fh23hnf23&redirect_uri=https://localhost:44380/Home/Index
           
            Header header = new Header();
            header.typ = "JWT";
            header.alg = "RS256";
            var hdy = JsonSerializer.Serialize(header);
            //Body bdy = new Body();
            string intergrationKey = "0e7585c5-11e4-4111-8bed-c81cc16ab207";
            string userId = "b73f14ef-ea39-436d-bf73-b4c326867f3d";
            string url = "account-d.docusign.com";
            string scope = "signature impersonation";
            TimeSpan createdTime = DateTime.UtcNow - new DateTime(2020, 8, 28);
            int createdT = (int)createdTime.TotalSeconds;
            TimeSpan expiryTime = DateTime.UtcNow - new DateTime(2020, 8, 29);
            int expiryT = (int)expiryTime.TotalSeconds;
            Body body = new Body();
            body.iss = intergrationKey;
            body.sub = userId;
            body.iat = createdT;
            body.exp = expiryT;
            body.aud = url;
            body.scope = scope;
            var bdy = JsonSerializer.Serialize(body);
            string bodyEncoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(bdy));
            string headerEncoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(hdy));
            string sign = headerEncoded + "." + bodyEncoded;
            byte[] dataToEncrypt = Encoding.UTF8.GetBytes(sign);

            

            //Encoding.ASCII.GetBytes(sign);
            byte[] encryptedData;

            //Create a new instance of RSACryptoServiceProvider to generate
            //public and private key data.
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(4096))
            {

                //Pass the data to ENCRYPT, the public key information 
                //(using RSACryptoServiceProvider.ExportParameters(false),
                //and a boolean flag specifying no OAEP padding.
                
                encryptedData = RSAEncrypt(dataToEncrypt, RSA.ExportParameters(false), false);

                //Pass the data to DECRYPT, the private key information 
                //(using RSACryptoServiceProvider.ExportParameters(true),
                //and a boolean flag specifying no OAEP padding.
                //decryptedData = RSADecrypt(encryptedData, RSA.ExportParameters(true), false);

                //Display the decrypted plaintext to the console. 
                //Console.WriteLine("Decrypted plaintext: {0}", ByteConverter.GetString(decryptedData));
            }
            string signature = Convert.ToBase64String(encryptedData);

            string jwt = headerEncoded + "." + bodyEncoded + "." + signature;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://account-d.docusign.com/oauth/token");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            DocusignRequest request = new DocusignRequest();
            request.grant_type = "urn:ietf:params:oauth:grant-type:jwt-bearer";
            request.assertion = jwt;
            var req = JsonSerializer.Serialize(request);
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                streamWriter.Write(req);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            return View();
        }
        public static byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            try
            {
                byte[] encryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {

                    //Import the RSA Key information. This only needs
                    //toinclude the public key information.
                    RSA.ImportParameters(RSAKeyInfo);

                    //Encrypt the passed byte array and specify OAEP padding.  
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.  
                    encryptedData = RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
                }
                return encryptedData;
            }
            //Catch and display a CryptographicException  
            //to the console.
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public class Header
        {
            public string typ { get; set; }
            public string alg { get; set; }
        }
        public class Body
        {
            public string iss { get; set; }
            public string sub { get; set; }
            public int iat { get; set; }
            public int exp { get; set; }
            public string aud { get; set; }
            public string scope { get; set; }
        }
        public class DocusignRequest
        {
            public string grant_type { get; set; }
            public string assertion { get; set; }
        }
    }
}
